import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import Helmet from "react-helmet";
import Contact from "../components/Contact";
import Footer from "../components/Footer";
import Header from "../components/Header";
import Info from "../components/Info";
import Projects from "../components/Projects";
import Strengths from "../components/Strengths";
import "./styles.css";

const Index = () => {
	return (
		<>
			<Helmet>
				<title>Portfolio - Anderson Botega</title>
			</Helmet>

			<Header />
			<Info />
			<Strengths />
			<Projects />
			<Contact />
			<Footer />
		</>
	);
};

export default Index;
