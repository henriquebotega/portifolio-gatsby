import React from "react";
import "./styles.css";

const Header = () => {
	return (
		<div className="contentHeader" id="contentHeader">
			<div className="linha">
				<div className="menuTop">
					<div className="linkedin"></div>
					<div className="titleTop">
						<a href="https://www.linkedin.com/in/andersonbotega/">anderson.botega</a>
					</div>
					<div className="telephone"></div>
					<div className="titleTop">
						<a alt="Call Me" href="tel://+12365126213">
							+1 (236) 512-6213
						</a>
					</div>
				</div>
			</div>

			<div className="linha">
				<div className="leftSide">
					<div className="contentSide">
						<div className="linha">
							<a href="#Info" className="btn">
								About me
							</a>
							<a href="#Strengths" className="btn">
								Strengths
							</a>
						</div>
					</div>
				</div>

				<div className="centerSide">
					<div className="foto"></div>
				</div>

				<div className="rightSide">
					<div className="contentSide">
						<div className="linha">
							<a href="#Projects" className="btn">
								Projects
							</a>
							<a href="#Contact" className="btn">
								Contact
							</a>
						</div>
					</div>
				</div>
			</div>

			<div className="linha">
				<div className="menuBottom">
					<a href="#Info" className="btn">
						About me
					</a>
					<a href="#Strengths" className="btn">
						Strengths
					</a>
					<a href="#Projects" className="btn">
						Projects
					</a>
					<a href="#Contact" className="btn">
						Contact
					</a>
				</div>
			</div>
		</div>
	);
};

export default Header;
