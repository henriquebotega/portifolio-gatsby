import React, { useState } from "react";
import ReactCardFlip from "react-card-flip";
import "./styles.css";

const Projects = () => {
	const orderar = (col) => {
		return col.sort((a, b) => (a["id"] > b["id"] ? 1 : -1));
	};

	const [proj, setProj] = useState(
		orderar([
			{
				id: 1,
				title: "Analla Store",
				type: "App",
				mobile: "React Native, StyleSheet",
				frontend: null,
				backend: "PHP",
				db: "MySQL",
				icons: ["react-native.png", "mysql.png"],
				image: "analla.jpg",
				url: "https://gitlab.com/henriquebotega/analla",
				isFlipped: false,
			},
			{
				id: 2,
				title: "Weather",
				type: "Website",
				mobile: null,
				frontend: "React, Bootstrap",
				backend: "API OpenWeatherMap",
				db: null,
				icons: ["react.png", "bootstrap.png"],
				image: "weather.png",
				url: "https://gitlab.com/henriquebotega/weather-finder",
				isFlipped: false,
			},
			{
				id: 3,
				title: "Bitcoin Monitor",
				type: "Website",
				mobile: null,
				frontend: "React, Css",
				backend: "API MercadoBitcoin",
				db: null,
				icons: ["react.png"],
				image: "bitcoin.jpg",
				url: "https://gitlab.com/henriquebotega/bitcoin-monitor",
				isFlipped: false,
			},
			{
				id: 4,
				title: "VanHack",
				type: "App",
				mobile: "React Native, Redux, Socket.IO, StyleSheet",
				frontend: null,
				backend: "Node, API Vanhack",
				db: null,
				icons: ["react-native.png", "socket-io.png"],
				image: "vanhack.jpg",
				url: "https://gitlab.com/henriquebotega/vanhack-mobile",
				isFlipped: false,
			},
			{
				id: 5,
				title: "Calculator",
				type: "App",
				mobile: "React Native, Expo, StyleSheet",
				frontend: null,
				backend: null,
				db: null,
				icons: ["react-native.png", "expo.png"],
				image: "calculator.jpg",
				url: "https://gitlab.com/henriquebotega/calculator2",
				isFlipped: false,
			},
			{
				id: 6,
				title: "Calistimer",
				type: "App",
				mobile: "React Native, StyleSheet",
				frontend: null,
				backend: null,
				db: null,
				icons: ["react-native.png"],
				image: "calistimer.jpg",
				url: "https://gitlab.com/devpleno/CalisTimer",
				isFlipped: false,
			},
			{
				id: 7,
				title: "KissRadio",
				type: "App",
				mobile: "React Native, WebView",
				frontend: null,
				backend: null,
				db: null,
				icons: ["react-native.png"],
				image: "kissradio.jpg",
				url: "https://gitlab.com/henriquebotega/kissradio",
				isFlipped: false,
			},
			{
				id: 8,
				title: "NuBank",
				type: "App",
				mobile: "React Native, Styled Components, QRCode",
				frontend: null,
				backend: null,
				db: null,
				icons: ["react-native.png"],
				image: "nubank.jpg",
				url: "https://gitlab.com/rocket-seat/nubank",
				isFlipped: false,
			},
			{
				id: 9,
				title: "Quiz",
				type: "App",
				mobile: "React Native, Expo",
				frontend: null,
				backend: null,
				db: "Local data JSON",
				icons: ["react-native.png", "expo.png", "json.png"],
				image: "quiz.jpg",
				url: "https://gitlab.com/henriquebotega/quiz2",
				isFlipped: false,
			},
			{
				id: 10,
				title: "Realm DB",
				type: "App",
				mobile: "React Native, Styled Components",
				frontend: null,
				backend: null,
				db: "Realm",
				icons: ["react-native.png", "realm-db.png"],
				image: "realmDB.jpg",
				url: "https://gitlab.com/henriquebotega/realmapp",
				isFlipped: false,
			},
			{
				id: 11,
				title: "Rocketfy",
				type: "Website",
				mobile: "React, Styled Components, Immer",
				frontend: null,
				backend: null,
				db: "Local data JSON",
				icons: ["react.png", "combo.jpg", "json.png"],
				image: "rocketfy.jpg",
				url: "https://gitlab.com/rocket-seat/rocketfy",
				isFlipped: false,
			},
			{
				id: 12,
				title: "Timer",
				type: "App",
				mobile: "React Native, Expo, StyleSheet",
				frontend: null,
				backend: null,
				db: null,
				icons: ["react-native.png", "expo.png"],
				image: "timer.jpg",
				url: "https://gitlab.com/henriquebotega/timer2",
				isFlipped: false,
			},
			{
				id: 13,
				title: "Trip Planner",
				type: "App",
				mobile: "React Native, StyleSheet, Maps",
				frontend: null,
				backend: null,
				db: "AsyncStorage",
				icons: ["react-native.png"],
				image: "tripplanner.jpg",
				url: "https://gitlab.com/devpleno/trip-planner",
				isFlipped: false,
			},
			{
				id: 14,
				title: "Portfolio",
				type: "Website",
				mobile: null,
				frontend: "React, Gatsby, Bootstrap",
				backend: null,
				image: "portfolio.jpg",
				db: null,
				icons: ["react.png", "gatsby.png", "bootstrap.png"],
				url: "https://gitlab.com/henriquebotega/portifolio-gatsby",
				isFlipped: false,
			},
			{
				id: 15,
				title: "Sua Radio Online",
				type: "App, Website",
				mobile: "React Native, Socket.IO",
				frontend: "React",
				backend: "Node",
				db: "MongoDB",
				icons: ["react-native.png", "mongodb.jpg", "socket-io.png"],
				image: "sua-radio.jpg",
				url: "https://gitlab.com/henriquebotega/sua-radio-mobile",
				isFlipped: false,
			},
			{
				id: 16,
				title: "Food",
				type: "App",
				mobile: "React Native, StyleSheet",
				frontend: null,
				backend: "Node",
				db: "MongoDB",
				icons: ["react-native.png", "node.png", "mongodb.jpg"],
				image: "food.jpg",
				url: "https://gitlab.com/henriquebotega/foodmobile",
				isFlipped: false,
			},
			{
				id: 17,
				title: "Spotify",
				type: "Website",
				mobile: null,
				frontend: "React, StyleSheet",
				backend: "Node",
				db: "Local data JSON",
				icons: ["react.png", "node.png", "json.png", "combo.jpg"],
				image: "spotify.jpg",
				url: "https://gitlab.com/henriquebotega/react-playlist-spotify",
				isFlipped: false,
			},
			{
				id: 18,
				title: "Grocery",
				type: "Website",
				mobile: null,
				frontend: "Angular, Material UI",
				backend: null,
				db: "Local data JSON",
				icons: ["angular.png", "material-ui.png", "json.png", "combo.jpg"],
				image: "my-grocery.jpg",
				url: "https://gitlab.com/henriquebotega/my-grocery",
				isFlipped: false,
			},
			{
				id: 19,
				title: "Million Game",
				type: "Website",
				mobile: null,
				frontend: "React, CSS",
				backend: "Node",
				db: "MongoDB",
				icons: ["react.png", "bootstrap.png", "node.png", "json.png", "combo.jpg"],
				image: "million-game.jpg",
				url: "https://gitlab.com/henriquebotega/million-game-frontend",
				isFlipped: false,
			},
			{
				id: 20,
				title: "Be the Hero",
				type: "App, Website",
				mobile: "React Native",
				frontend: "React, CSS",
				backend: "Node",
				db: "Knex",
				icons: ["react.png", "react-native.png", "node.png", "json.png", "combo.jpg"],
				image: "be-hero.jpg",
				url: "https://gitlab.com/henriquebotega/be-hero-frontend",
				isFlipped: false,
			},
			{
				id: 21,
				title: "My Calendar",
				type: "Website",
				mobile: "Angular",
				frontend: "CSS",
				backend: null,
				db: null,
				icons: ["angular.png", "combo.jpg"],
				image: "my-calendar.png",
				url: "https://gitlab.com/henriquebotega/my-calendar",
				isFlipped: false,
			},
			{
				id: 22,
				title: "Sabium ERP",
				type: "Website",
				mobile: null,
				frontend: "AngularJs, Material UI",
				backend: "Delphi",
				db: "PostgreSQL",
				icons: ["angular.png", "combo.jpg", "gulp.png", "jasmine.png", "postgre.png", "sass.png"],
				image: "sabium.png",
				url: "https://gitlab.com/henriquebotega",
				isFlipped: false,
			},
		])
	);

	const setFlipped = (obj) => {
		let novoObj = { ...obj, isFlipped: !obj.isFlipped };
		setProj(orderar([...proj.filter((item) => item.id !== obj.id), novoObj]));
	};

	return (
		<>
			<div className="msgInfo" name="Projects" id="Projects">
				<h3>Projects</h3>
			</div>

			<div className="contentProjects">
				{proj.reverse().map((obj, i) => {
					return (
						<div className="cardFlip" key={i}>
							<ReactCardFlip isFlipped={obj.isFlipped}>
								<div role="button" tabIndex={0} className="myCard myCardFront" onClick={() => setFlipped(obj)} onKeyDown={() => setFlipped(obj)} style={{ backgroundImage: `url(${require("../../images/proj/" + obj.image)})`, width: 225, height: 225, backgroundSize: "cover", backgroundRepeat: "no-repeat" }}>
									<span className="cardTitle">{obj.title}</span>

									<span className="cardIcons">
										{obj.icons.map((img, k) => {
											return <img key={k} src={require("../../images/strengths/" + img)} className="iconeProj" alt="" />;
										})}
									</span>
								</div>

								<div role="button" tabIndex={0} className="myCard myCardBack" onClick={() => setFlipped(obj)} onKeyDown={() => setFlipped(obj)}>
									{obj.type && (
										<span className="titleCardBack">
											<b>Type</b> <p>{obj.type}</p>
										</span>
									)}
									{obj.mobile && (
										<span className="titleCardBack">
											<b>Mobile</b> <p>{obj.mobile}</p>
										</span>
									)}
									{obj.frontend && (
										<span className="titleCardBack">
											<b>Front-end</b> <p>{obj.frontend}</p>
										</span>
									)}
									{obj.backend && (
										<span className="titleCardBack">
											<b>Back-end</b> <p>{obj.backend}</p>
										</span>
									)}
									{obj.db && (
										<span className="titleCardBack">
											<b>Database</b> <p>{obj.db}</p>
										</span>
									)}

									<a href={`${obj.url}`} target="_blank" className="posGit" rel="noopener noreferrer">
										<img src={require("../../images/strengths/git.png")} className="iconGit" alt="Check it on Gitlab" />
									</a>
								</div>
							</ReactCardFlip>
						</div>
					);
				})}

				<div className="clear"></div>
			</div>
		</>
	);
};

export default Projects;
