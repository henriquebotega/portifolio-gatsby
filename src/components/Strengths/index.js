import React, { useState } from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import "./styles.css";

const Strengths = () => {
	const [colItems] = useState([
		{ id: 1, title: "Angular", time: 4, image: "angular.png" },
		{ id: 2, title: "Bootstrap", time: 2, image: "bootstrap.png" },
		{ id: 3, title: "HTML + JS + CSS", time: 7, image: "combo.jpg" },
		{ id: 4, title: "Docker", time: 1, image: "docker.png" },
		{ id: 5, title: "ExtJs", time: 3, image: "extjs.png" },
		{ id: 6, title: "Git", time: 4, image: "git.png" },
		{ id: 7, title: "Gulp", time: 3, image: "gulp.png" },
		{ id: 8, title: "Jasmine", time: 1, image: "jasmine.png" },
		{ id: 9, title: "Jest", time: 1, image: "jest.png" },
		{ id: 10, title: "Material UI", time: 1, image: "material-ui.png" },
		{ id: 11, title: "MongoDB", time: 2, image: "mongodb.jpg" },
		{ id: 12, title: "MySQL", time: 4, image: "mysql.png" },
		{ id: 13, title: "Node", time: 2, image: "node.png" },
		{ id: 14, title: "Postgre", time: 4, image: "postgre.png" },
		{ id: 15, title: "Protractor", time: 1, image: "protractor.png" },
		{ id: 16, title: "React Native", time: 2, image: "react-native.png" },
		{ id: 17, title: "React", time: 2, image: "react.png" },
		{ id: 18, title: "Sass", time: 1, image: "sass.png" },
		{ id: 19, title: "Ubuntu", time: 7, image: "ubuntu.png" },
		{ id: 20, title: "Intellij Idea", time: 3, image: "intellij.png" },
		{ id: 21, title: "Visual Studio Code", time: 4, image: "vscode.png" },
		{ id: 22, title: "Expo", time: 2, image: "expo.png" },
		{ id: 23, title: "Firebase", time: 1, image: "firebase.png" },
		{ id: 24, title: "Gatsby", time: 1, image: "gatsby.png" },
		{ id: 25, title: "Heroku", time: 2, image: "heroku.png" },
		{ id: 26, title: "Json", time: 4, image: "json.png" },
		{ id: 27, title: "Netlify", time: 1, image: "netlify.png" },
		{ id: 28, title: "RealmDB", time: 1, image: "realm-db.png" },
		{ id: 29, title: "Socket-IO", time: 2, image: "socket-io.png" }
	]);

	return (
		<>
			<div className="msgInfo" name="Strengths" id="Strengths">
				<h3>Strengths</h3>
			</div>

			<div className="contentStrengths">
				{colItems
					.sort((a, b) => (a["time"] < b["time"] ? 1 : -1))
					.map((obj, i) => {
						return (
							<OverlayTrigger
								key={i}
								placement="bottom"
								overlay={
									<Tooltip id={`tooltip-${i}`}>
										<h5>{obj.title}</h5>
										{obj.time} {obj.time === 1 ? `year` : `years`}
									</Tooltip>
								}
							>
								<img src={require("../../images/strengths/" + obj.image)} className="icone" alt="" />
							</OverlayTrigger>
						);
					})}
			</div>

			<div className="clear"></div>
		</>
	);
};

export default Strengths;
