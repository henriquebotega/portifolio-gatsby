import React, { useEffect, useState } from "react";
import "./styles.css";

const Footer = () => {
	const [exibirTop, setExibirTop] = useState(false);

	useEffect(() => {
		window.addEventListener("scroll", handleScroll);

		return () => {
			window.removeEventListener("scroll", handleScroll);
		};
	}, []);

	const handleScroll = (event) => {
		let position = window.scrollY;
		setExibirTop(false);

		if (position > 200) {
			setExibirTop(true);
		}
	};

	setTimeout(() => {
		if (window.location.hash) {
			const pg = window.location.hash.split("#").filter((i) => i !== "");
			const page = document.getElementById(pg[0]);

			if (page) {
				// page.scrollIntoView();
			}
		}
	}, 2000);

	const yearCurrent = new Date().getFullYear();

	return (
		<div className="contentFooter">
			{exibirTop && (
				<div
					className="gotTop"
					onClick={() => {
						window.scrollTo(0, 0);
						window.location.hash = "";
					}}
				>
					<img src={require("../../images/top.png")} alt="" style={{ width: 25, height: 35 }} />
				</div>
			)}

			<div className="leftSide">
				<h5>Anderson Botega</h5>
				<div>Intermediate Frontend</div>
				<b>Copyright {yearCurrent}</b>
			</div>

			<div className="rightSide">
				<h2>Contact</h2>
				<div className="textSm">
					<a alt="Call Me" href="tel://+12365126213">
						+1 (236) 512-6213
					</a>
				</div>
				<div className="textSm">henriquebotega@gmail.com</div>
				<div className="textSm">
					<img src={require("../../images/skype.png")} alt="" style={{ width: 25, height: 25 }} />
					<a alt="Chat with Me" href="skype:anderson.henrique.botega?chat">
						anderson.henrique.botega
					</a>
				</div>
			</div>
		</div>
	);
};

export default Footer;
