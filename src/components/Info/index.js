import React from "react";
import "./styles.css";

const Info = () => {
	return (
		<>
			<div className="contentInfo" name="Info" id="Info">
				<img src={require("../../images/info.png")} alt="My Strengths" style={{ width: "90vw", height: "auto" }} />
			</div>
		</>
	);
};

export default Info;
