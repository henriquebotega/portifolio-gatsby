import React, { useState } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import "./styles.css";

const Contact = () => {
	const [formulario, setFormulario] = useState({ name: "", email: "", message: "" });
	const [enviar, setEnviar] = useState(false);

	const defineValor = e => {
		setFormulario({
			...formulario,
			[e.target.name]: e.target.value
		});
	};

	const enviarForm = e => {
		e.preventDefault();
		setEnviar(true);

		const subject = `Contact by portfolio`;

		let body = `
      Name: ${formulario.name}
      E-mail: ${formulario.email}
      Message: ${formulario.message}
    `;

		body = body.replace(/"/g, "%22");
		body = body.replace(/&/g, "%26");

		window.open("mailto:henriquebotega@gmail.com?subject=" + subject + "&body=" + encodeURIComponent(body));

		setFormulario({ name: "", email: "", message: "" });

		setTimeout(() => {
			setEnviar(false);
		}, 3000);
	};

	return (
		<>
			<div className="msgInfo" name="Contact" id="Contact">
				<h3>Contact</h3>
			</div>

			<div className="contentContact">
				<Form>
					<Row>
						<Col md>
							<Form.Group controlId="formName">
								<Form.Label>Name</Form.Label>
								<Form.Control placeholder="Enter your name" value={formulario.name} name="name" onChange={e => defineValor(e)} />
							</Form.Group>
						</Col>
						<Col md>
							<Form.Group controlId="formEmail">
								<Form.Label>Email</Form.Label>
								<Form.Control type="email" placeholder="Enter your email" value={formulario.email} name="email" onChange={e => defineValor(e)} />
							</Form.Group>
						</Col>
					</Row>

					<Form.Group controlId="formMessage">
						<Form.Label>Message</Form.Label>
						<Form.Control as="textarea" rows="3" placeholder="Enter your message" value={formulario.message} name="message" onChange={e => defineValor(e)} />
					</Form.Group>

					<Button variant="primary" className="btnSend" type="submit" onClick={e => enviarForm(e)}>
						Send
					</Button>
				</Form>

				{enviar && (
					<div className="msgSuccess">
						<h3>Information</h3>
						<p>Your comment was submitted successfully</p>
					</div>
				)}
			</div>
		</>
	);
};

export default Contact;
